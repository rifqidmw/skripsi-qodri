package com.skripsi.catscanner.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.skripsi.catscanner.R;

import javax.xml.transform.Result;

public class ResultActivity extends AppCompatActivity {

    private BottomSheetBehavior sheetBehavior;
    private ConstraintLayout bottom_sheet;
    private TextView tvName, tvType, tvInfo, tvDetailDescription, tvProbability, tvTimer;
    private Button btnExit, btnScan, btnMore;
    private ConstraintLayout lyBottomsheet;
    private ImageView imgArrow, imgResult, imgGradient;

    private String name, type, probability, timer;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        bottom_sheet = findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);

        tvName = findViewById(R.id.tv_name);
        tvType = findViewById(R.id.tv_type);
        tvInfo = findViewById(R.id.tv_info);
        tvDetailDescription = findViewById(R.id.tv_deskripsi);
        tvProbability = findViewById(R.id.tv_probability);
        tvTimer = findViewById(R.id.tv_timer);
        btnExit = findViewById(R.id.btn_exit);
        btnScan = findViewById(R.id.btn_scan);
        btnMore = findViewById(R.id.btn_more);
        imgArrow = findViewById(R.id.img_arrow);
        imgResult = findViewById(R.id.img_result);
        imgGradient = findViewById(R.id.img_gradient);

        Intent getIntent = getIntent();
        name = getIntent.getStringExtra("NAME");
        type = getIntent.getStringExtra("TYPE");
        probability = getIntent.getStringExtra("PROBABILITY");
        timer = getIntent.getStringExtra("TIMER");

        tvProbability.setText(probability);
        tvTimer.setText("Time: "+timer+" detik");

        if (name.equals("Anggora")){
            tvName.setText("Kucing Anggora");
            tvType.setText(type);
            tvDetailDescription.setText(getResources().getString(R.string.kucing_anggora));
            if (type.equals("Hitam")){
                Glide.with(ResultActivity.this).load(R.drawable.img_result_anggora_hitam).into(imgResult);
            } else if (type.equals("Putih")) {
                Glide.with(ResultActivity.this).load(R.drawable.img_result_anggora_putih).into(imgResult);
            } else if (type.equals("Calico")){
                Glide.with(ResultActivity.this).load(R.drawable.img_result_anggora_calico).into(imgResult);
            }
        } else if (name.equals("Persia")){
            tvName.setText("Kucing Persian");
            tvType.setText(type);
            tvType.setVisibility(View.VISIBLE);
            tvDetailDescription.setText(getResources().getString(R.string.kucing_persia));
            if (type.equals("Flatnose")){
                Glide.with(ResultActivity.this).load(R.drawable.img_result_persia_flatnose).into(imgResult);
            } else if (type.equals("Peaknose")){
                Glide.with(ResultActivity.this).load(R.drawable.img_result_persia_peaknose).into(imgResult);
            } else if (type.equals("Himalaya")){
                Glide.with(ResultActivity.this).load(R.drawable.img_result_persia_himalaya).into(imgResult);
            }
        }

        btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, WebviewActivity.class);
                intent.putExtra("NAME", name);
                startActivity(intent);
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, ClassifierActivity.class);
                startActivity(intent);
                ResultActivity.this.finish();
            }
        });

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        tvInfo.setVisibility(View.GONE);
                        imgArrow.setVisibility(View.GONE);
                        imgGradient.setVisibility(View.GONE);
                        bottom_sheet.setBackgroundColor(ContextCompat.getColor(ResultActivity.this, R.color.colorWhite));
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        tvInfo.setVisibility(View.VISIBLE);
                        imgArrow.setVisibility(View.VISIBLE);
                        imgGradient.setVisibility(View.VISIBLE);
//                        bottom_sheet.setBackground(ContextCompat.getDrawable(ResultActivity.this, R.drawable.gradient_shape));
                        bottom_sheet.setBackgroundColor(ContextCompat.getColor(ResultActivity.this, R.color.colorBlue));
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
    }
}