package com.skripsi.catscanner.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.media.ImageReader;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.util.Size;
import android.util.TypedValue;
import android.view.Display;
import android.widget.Toast;

import com.skripsi.catscanner.CameraActivity;
import com.skripsi.catscanner.Classifier;
import com.skripsi.catscanner.MSCognitiveServicesClassifier;
import com.skripsi.catscanner.OverlayView;
import com.skripsi.catscanner.R;
import com.skripsi.catscanner.TimerTextHelper;
import com.skripsi.catscanner.env.BorderedText;
import com.skripsi.catscanner.env.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

public class ClassifierActivity extends CameraActivity implements ImageReader.OnImageAvailableListener {
    private static final Logger LOGGER = new Logger();

    private static final Size DESIRED_PREVIEW_SIZE = new Size(640, 480);

    private Integer sensorOrientation;
    private MSCognitiveServicesClassifier classifier;

    private BorderedText borderedText;
    private MediaPlayer mp;
    private Context context = this;
    private String probability = "";
    private String timer = "";

    @Override
    protected int getLayoutId() {
        return R.layout.camera_connection_fragment;
    }

    @Override
    protected Size getDesiredPreviewFrameSize() {
        return DESIRED_PREVIEW_SIZE;
    }

    private static final float TEXT_SIZE_DIP = 10;

    @Override
    public void onPreviewSizeChosen(final Size size, final int rotation) {

        // pengaturan untuk text hasil
        final float textSizePx = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, getResources().getDisplayMetrics());
        borderedText = new BorderedText(textSizePx);
        borderedText.setTypeface(Typeface.MONOSPACE);

        // inisialisasi MSCognitiveServicesClassifier class di activity ini menjadi classifier
        classifier = new MSCognitiveServicesClassifier(ClassifierActivity.this);

        //get width & height
        previewWidth = size.getWidth();
        previewHeight = size.getHeight();


        final Display display = getWindowManager().getDefaultDisplay();
        final int screenOrientation = display.getRotation();


        //menampilkan log untuk sensor orientation
        LOGGER.i("Sensor orientation: %d, Screen orientation: %d", rotation, screenOrientation);

        sensorOrientation = rotation + screenOrientation;

        //menampilkan log untuk size
        LOGGER.i("Initializing at size %dx%d", previewWidth, previewHeight);
        rgbFrameBitmap = Bitmap.createBitmap(previewWidth, previewHeight, Bitmap.Config.ARGB_8888);

        yuvBytes = new byte[3][];

        //perintah untuk render setiap mendeteksi object
        addCallback(
                new OverlayView.DrawCallback() {
                    @Override
                    public void drawCallback(final Canvas canvas) {
                        renderDebug(canvas);
                    }
                });
    }

    @Override
    public void onBackPressed(){
        startActivity(new Intent(ClassifierActivity.this, MainActivity.class));
        ClassifierActivity.this.finish();
    }

    //proses yg akan dijalankan setiap render
    protected void processImageRGBbytes(int[] rgbBytes) {
        //pengaturan frame untuk pixel
        rgbFrameBitmap.setPixels(rgbBytes, 0, previewWidth, 0, 0, previewWidth, previewHeight);


        //di belakang layar sistem, proses identifikasi pola disetting akan dijalankan setiap 3 detik dengan handler
        runInBackground(
                new Runnable() {
                    @Override
                    public void run() {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                TimerTextHelper timerTextHelper = new TimerTextHelper();
                                timerTextHelper.start();

                                //untuk mendapatkan info waktu terjadi render
                                final long startTime = SystemClock.uptimeMillis();
                                Classifier.Recognition r = classifier.classifyImage(rgbFrameBitmap, sensorOrientation);
                                lastProcessingTimeMs = SystemClock.uptimeMillis() - startTime;

                                //hasil identifikasi akan di simpan di arraylist results
                                final List<Classifier.Recognition> results = new ArrayList<>();

                                //jika hasil identifikasi memiliki tingkat akurasi 70& maka hasilnya akan disimpan di array results
                                if (r.getConfidence() > 0.7) {
                                    results.add(r);
                                    timerTextHelper.stop();
                                    long elapsedTime = timerTextHelper.getElapsedTime();
                                    long seconds = TimeUnit.MILLISECONDS.toSeconds(elapsedTime);
                                    timer = seconds+"."+elapsedTime;
                                }

                                //log untuk menampilkan hasil deteksi
                                LOGGER.i("Detect: %s", results);

                                String result = results.toString();
                                String[] split = result.split(" ");
                                if (!results.isEmpty()){
                                    probability = split[4].replace("(", "").replace(")", "").replace("]", "");
                                }
                                Log.i("TensonFlow","Detect Probability: "+ probability);
                                if (resultsView == null) {
//                                    resultsView = findViewById(R.id.results);
                                }
//                                resultsView.setResults(results);


                                Intent intent = new Intent(ClassifierActivity.this, ResultActivity.class);
                                if (LOGGER.result("Detect: %s",results).contains("Anggora - Hitam")){
                                    intent.putExtra("NAME", "Anggora");
                                    intent.putExtra("TYPE", "Hitam");
                                    intent.putExtra("PROBABILITY", probability);
                                    intent.putExtra("TIMER", timer);
                                    startActivity(intent);
                                } else if (LOGGER.result("Detect: %s",results).contains("Anggora - Putih")){
                                    intent.putExtra("NAME", "Anggora");
                                    intent.putExtra("TYPE", "Putih");
                                    intent.putExtra("PROBABILITY", probability);
                                    intent.putExtra("TIMER", timer);
                                    startActivity(intent);
                                } else if (LOGGER.result("Detect: %s",results).contains("Anggora - Calico")){
                                    intent.putExtra("NAME", "Anggora");
                                    intent.putExtra("TYPE", "Calico");
                                    intent.putExtra("PROBABILITY", probability);
                                    intent.putExtra("TIMER", timer);
                                    startActivity(intent);
                                } else if (LOGGER.result("Detect: %s",results).contains("Persia - Flatnose")){
                                    intent.putExtra("NAME", "Persia");
                                    intent.putExtra("TYPE", "Flatnose");
                                    intent.putExtra("PROBABILITY", probability);
                                    intent.putExtra("TIMER", timer);
                                    startActivity(intent);
                                } else if (LOGGER.result("Detect: %s",results).contains("Persia - Peaknose")){
                                    intent.putExtra("NAME", "Persia");
                                    intent.putExtra("TYPE", "Peaknose");
                                    intent.putExtra("PROBABILITY", probability);
                                    intent.putExtra("TIMER", timer);
                                    startActivity(intent);
                                } else if (LOGGER.result("Detect: %s",results).contains("Persia - Himalaya")){
                                    intent.putExtra("NAME", "Persia");
                                    intent.putExtra("TYPE", "Himalaya");
                                    intent.putExtra("PROBABILITY", probability);
                                    intent.putExtra("TIMER", timer);
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(context, "Tidak dapat mengenali", Toast.LENGTH_SHORT).show();
                                }

                                requestRender();
                                computing = false;
                                if (postInferenceCallback != null) {
                                    postInferenceCallback.run();
                                }
                            }
                        }, 3000);

                    }
                });

    }

    @Override
    public void onSetDebug(boolean debug) {
    }

    private void renderDebug(final Canvas canvas) {
        if (!isDebug()) {
            return;
        }

        final Vector<String> lines = new Vector<String>();
        lines.add("Inference time: " + lastProcessingTimeMs + "ms");
        borderedText.drawLines(canvas, 10, canvas.getHeight() - 10, lines);
    }
}