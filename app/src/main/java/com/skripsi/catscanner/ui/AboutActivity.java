package com.skripsi.catscanner.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.skripsi.catscanner.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView tvTitle = findViewById(R.id.tv_title);
        Typeface customfont = Typeface.createFromAsset(getAssets(),"font/Dattge Hurty.otf");
        tvTitle.setTypeface(customfont);

        ImageView btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                AboutActivity.this.finish();
            }
        });
    }
}