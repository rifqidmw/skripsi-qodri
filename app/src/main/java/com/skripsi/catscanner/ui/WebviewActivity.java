package com.skripsi.catscanner.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.skripsi.catscanner.R;

public class WebviewActivity extends AppCompatActivity {

    SwipeRefreshLayout swipeRefreshLayout;
    WebView webView;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);
        webView = findViewById(R.id.web_view);

        Intent getIntent = getIntent();
        name = getIntent.getStringExtra("NAME");

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadWeb(name);
            }
        });

        loadWeb(name);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void loadWeb(String name){
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        if (name.equals("Persia")){
            webView.loadUrl("https://id.wikipedia.org/wiki/Kucing_persia#:~:text=Kucing%20persia%20adalah%20ras%20kucing,Serikat%20usai%20Perang%20Dunia%20II.");
        } else if (name.equals("Anggora")){
            webView.loadUrl("https://id.wikipedia.org/wiki/Anggora_turki#:~:text=Anggora%20adalah%20kucing%20dengan%20ciri,%2C%20lebar%2C%20dan%20berbentuk%20segitiga.");
        }
        swipeRefreshLayout.setRefreshing(true);
        webView.setWebViewClient(new WebViewClient(){
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                webView.loadUrl("file:///android_asset/error.html");
            }
            public  void  onPageFinished(WebView view, String url){
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if(webView.getProgress()== 100){
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    swipeRefreshLayout.setRefreshing(true);
                }
            }
        });
    }

    @Override
    public void onBackPressed(){

        if (webView.canGoBack()){
            webView.goBack();
        }else {
            finish();
        }
    }
}